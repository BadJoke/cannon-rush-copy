﻿using DG.Tweening;
using UnityEngine;

public class OptionsLoader : MonoBehaviour
{
    [SerializeField] private int _level;

    [Header("Sens")]
    [SerializeField] private float _xSensitivity;
    [SerializeField] private float _ySensitivity;

    [Header("Controlling")]
    [SerializeField] private float _maxXAngle;
    [SerializeField] private float _minXAngle;
    [SerializeField] private float _maxYAngle;

    [Header("Shooting")]
    [SerializeField] private float _shootForce;
    [SerializeField] private float _shootDelay;

    [Header("Primary explosion")]
    [SerializeField] private float _primaryForce;
    [SerializeField] private float _primaryRadius;

    [Header("Secondary explosion")]
    [SerializeField] private float _secondaryForce;
    [SerializeField] private float _secondaryRadius;

    [Header("Enemy")]
    [SerializeField] private float _enemySpeed;
    [SerializeField] private float _enemySpawnDelay;
    [SerializeField] private float _lifeTimeAfterStrike;
    [SerializeField] private int _miniBossHealth;
    [SerializeField] private float _miniBossSpeed;
    [SerializeField] private int _bossHealth;
    [SerializeField] private float _bossSpeed;
    [SerializeField] private Vector2Int _frequenceMiniBossSpawn;

    private void Awake()
    {
        if (_level < 1)
        {
            if (PlayerPrefs.HasKey("Level"))
            {
                Options.CurrentLevel = PlayerPrefs.GetInt("Level");
            }
            else
            {
                Options.CurrentLevel = 1;
            }
        }
        else
        {
            if (_level > Options.CurrentLevel)
            {
                Options.CurrentLevel = _level;
            }
        }

        DOTween.KillAll();
        DOTween.SetTweensCapacity(300, 300);

        Options.XSensitivity = _xSensitivity * 0.01f;
        Options.YSensitivity = _ySensitivity * 0.01f;

        if (PlayerPrefs.HasKey("SensRatio"))
        {
            Options.SensRatio = PlayerPrefs.GetFloat("SensRatio");
        }
        else
        {
            Options.SensRatio = 1.5f;
        }

        Options.MaxXAngle = _maxXAngle;
        Options.MinXAngle = _minXAngle;
        Options.MaxYAngle = _maxYAngle;

        Options.ShootForce = _shootForce;
        Options.ShootDelay = _shootDelay;
        Options.CurrentShootDelay = _shootDelay;

        Options.PrimaryForce = _primaryForce;
        Options.PrimaryRadius = _primaryRadius;

        Options.SecondaryForce = _secondaryForce;
        Options.SecondaryRadius = _secondaryRadius;

        Options.EnemySpeed = _enemySpeed;
        Options.EnemySpawnDelay = _enemySpawnDelay;
        Options.LifeTimeAfterStrike = _lifeTimeAfterStrike;
        Options.MiniBossHealth = _miniBossHealth;
        Options.MiniBossSpeed = _miniBossSpeed;
        Options.BossHealth = _bossHealth;
        Options.BossSpeed = _bossSpeed;
        Options.FrequenceBossSpawn = _frequenceMiniBossSpawn;
    }
}
