﻿using UnityEngine;

public static class SpellOptions
{
    public static float SpeedUpDuration;
    public static float SpeedUpModifier;
    public static float SpeedUpCooldown;

    public static Vector2Int SpikesCount;
    public static float SpikesRadius;
    public static float SpikesDuration;
    public static float SpikesCooldown;
    public static int SpikesDamage;

    public static Color FreezeColor;
    public static float FreezeDuration;
    public static float FreezeCountdown;
    public static float FreezeCooldown;

    public static float SuperCannonballFadeInterval;
    public static float SuperCannonballFadeDuration;
    public static float SuperCannonballCooldown;
    public static int SuperCannonballDamage;
    public static int SuperCannonballHealth;

    public static Vector2Int FireballCount;
    public static float FireballSpeed;
    public static float FireballInterval;
    public static float FireballFadeInterval;
    public static float FireballFadeDuration;
    public static float FireballExplosionRadius;
    public static float FireballCooldown;
    public static int FireballDamage;
}
