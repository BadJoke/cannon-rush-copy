﻿using UnityEngine;

public static class Options
{
    public static float XSensitivity;
    public static float YSensitivity;
    public static float SensRatio;

    public static float MaxXAngle;
    public static float MinXAngle;
    public static float MaxYAngle;

    public static float ShootForce;
    public static float ShootDelay;
    public static float CurrentShootDelay;

    public static float PrimaryForce;
    public static float PrimaryRadius;

    public static float SecondaryForce;
    public static float SecondaryRadius;

    public static float EnemySpeed;
    public static float EnemySpawnDelay;
    public static float LifeTimeAfterStrike;
    public static int MiniBossHealth;
    public static float MiniBossSpeed;
    public static int BossHealth;
    public static float BossSpeed;
    public static Vector2Int FrequenceBossSpawn;

    public static int CurrentLevel;
    public static int Coins;
}
