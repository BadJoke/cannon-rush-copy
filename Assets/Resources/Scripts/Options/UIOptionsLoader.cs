﻿using UnityEngine;

public class UIOptionsLoader : MonoBehaviour
{
    [Header("Timer")]
    [SerializeField] private float _levelTime;
    [SerializeField] [Range(0, 1)] private float _spawnDisableTime;
    [SerializeField] private float _victoryDelay;

    private void Awake()
    {
        UIOptions.LevelTime = _levelTime;
        UIOptions.SpawnDisableTime = _spawnDisableTime;
        UIOptions.VictoryDelay = _victoryDelay;
    }
}
