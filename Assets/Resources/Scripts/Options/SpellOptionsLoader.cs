﻿using UnityEngine;

public class SpellOptionsLoader : MonoBehaviour
{
    [Header("Shooting Speed Up")]
    [SerializeField] private float _speedUpModifier;
    [SerializeField] private float _speedUpDuration;
    [SerializeField] private float _speedUpCooldown;

    [Header("Spikes")]
    [SerializeField] private Vector2Int _spikesCount;
    [SerializeField] private float _spikesRadius;
    [SerializeField] private float _spikesDuration;
    [SerializeField] private float _spikesCooldown;
    [SerializeField] private int _spikesDamage;

    [Header("Freeze")]
    [SerializeField] private Color _freezeColor;
    [SerializeField] private float _freezeDuration;
    [SerializeField] private float _freezeCooldown;

    [Header("Super Cannonball")]
    [SerializeField] private float _superCannonballFadeInterval;
    [SerializeField] private float _superCannonballFadeDuration;
    [SerializeField] private float _superCannonballCooldown;
    [SerializeField] private int _superCannonballDamage;
    [SerializeField] private int _superCannonballHealth;

    [Header("Fireball")]
    [SerializeField] private Vector2Int _fireballCount;
    [SerializeField] private float _fireballSpeed;
    [SerializeField] private float _fireballInterval;
    [SerializeField] private float _fireballFadeInterval;
    [SerializeField] private float _fireballFadeDuration;
    [SerializeField] private float _fireballExplosionRadius;
    [SerializeField] private float _fireballCooldown;
    [SerializeField] private int _fireballDamage;

    private void Awake()
    {
        SpellOptions.SpeedUpModifier = _speedUpModifier;
        SpellOptions.SpeedUpDuration = _speedUpDuration;
        SpellOptions.SpeedUpCooldown = _speedUpCooldown;

        SpellOptions.SpikesCount = _spikesCount;
        SpellOptions.SpikesRadius = _spikesRadius;
        SpellOptions.SpikesDuration = _spikesDuration;
        SpellOptions.SpikesCooldown = _spikesCooldown;
        SpellOptions.SpikesDamage = _spikesDamage;

        SpellOptions.FreezeColor = _freezeColor;
        SpellOptions.FreezeDuration = _freezeDuration;
        SpellOptions.FreezeCooldown = _freezeCooldown;

        SpellOptions.SuperCannonballFadeInterval = _superCannonballFadeInterval;
        SpellOptions.SuperCannonballFadeDuration = _superCannonballFadeDuration;
        SpellOptions.SuperCannonballCooldown = _superCannonballCooldown;
        SpellOptions.SuperCannonballDamage = _superCannonballDamage;
        SpellOptions.SuperCannonballHealth = _superCannonballHealth;

        SpellOptions.FireballCount = _fireballCount;
        SpellOptions.FireballSpeed = _fireballSpeed;
        SpellOptions.FireballInterval = _fireballInterval;
        SpellOptions.FireballFadeInterval = _fireballFadeInterval;
        SpellOptions.FireballFadeDuration = _fireballFadeDuration;
        SpellOptions.FireballExplosionRadius = _fireballExplosionRadius;
        SpellOptions.FireballCooldown = _fireballCooldown;
        SpellOptions.FireballDamage = _fireballDamage;
    }
}
