﻿using FUGames.Pool;
using System.Collections;
using UnityEngine;

public class Fire : MonoBehaviour
{
    [SerializeField] private Transform _shootPoint;
    [SerializeField] private ParticleSystem _explosion;
    [SerializeField] private Transform _container;
    [SerializeField] private PoolManager _pool;
    [SerializeField] private EnemyCounter _enemyCounter;
    [SerializeField] private Castle _castle;

    private Coroutine _shooting;

    private void OnEnable()
    {
        _castle.Lose += OnLose;
        _enemyCounter.EnemiesDestroyed += OnEnemiesDestroyed;
    }

    private void OnDisable()
    {
        _castle.Lose -= OnLose;
        _enemyCounter.EnemiesDestroyed -= OnEnemiesDestroyed;
    }

    private void Start()
    {
        StartShooting();
    }

    private IEnumerator Shooting()
    {
        while (true)
        {
            Shoot();
            yield return new WaitForSeconds(Options.CurrentShootDelay);
        }
    }

    private void OnEnemiesDestroyed()
    {
        StopShooting();
    }

    private void StartShooting()
    {
        _shooting = StartCoroutine(Shooting());
    }

    private void StopShooting()
    {
        StopCoroutine(_shooting);
    }

    private void Shoot()
    {
        var cannonball = _pool.Take(typeof(Cannonball)).transform;
        cannonball.GetComponent<Cannonball>().Refresh();
        cannonball.position = _shootPoint.position;
        cannonball.rotation = transform.rotation;
        cannonball.parent = _container;

        var rigidbody = cannonball.GetComponent<Rigidbody>();
        rigidbody.velocity = _shootPoint.forward * Options.ShootForce;

        _explosion.Play();
    }

    private void OnLose()
    {
        StopAllCoroutines();
        enabled = false;
    }
}
