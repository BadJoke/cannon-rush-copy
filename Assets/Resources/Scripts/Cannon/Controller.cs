﻿using UnityEngine;

public class Controller : MonoBehaviour
{
    [SerializeField] private Transform _barrel;
    [SerializeField] private Transform _target;
    [SerializeField] private Transform _shootPoint;
    [SerializeField] private Castle _castle;

    private Vector3 _startPosition;
    private Vector3 _endPosition;

    private float _maxXAngle;
    private float _minXAngle;
    private float _maxYAngle;

    private void OnEnable()
    {
        _castle.Lose += OnLose;
    }

    private void OnDisable()
    {
        _castle.Lose -= OnLose;
    }

    private void Awake()
    {
        Input.multiTouchEnabled = false;
    }

    private void Start()
    {
        _maxXAngle = Options.MaxXAngle;
        _minXAngle = Options.MinXAngle;
        _maxYAngle = Options.MaxYAngle;

        Targeting();
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            _startPosition = Input.mousePosition;
        }

        if (Input.GetMouseButton(0))
        {
            _endPosition = Input.mousePosition;
            RotateCannon();
            _startPosition = _endPosition;
        }
    }

    private void RotateCannon()
    {
        float yAngle = (_endPosition.x - _startPosition.x) * Options.XSensitivity * Options.SensRatio;
        float xAngle = (_startPosition.y - _endPosition.y) * Options.YSensitivity * Options.SensRatio;

        transform.Rotate(0f, yAngle, 0f);
        _barrel.Rotate(xAngle, 0f, 0f);

        if (_barrel.localEulerAngles.x > 180)
        {
            if (_barrel.localEulerAngles.x - 360 < _maxXAngle)
            {
                var rotation = Vector3.zero;
                rotation.x = _maxXAngle;
                _barrel.localEulerAngles = rotation;
            }
        }
        else
        {
            if (_barrel.localEulerAngles.x > _minXAngle)
            {
                var rotation = Vector3.zero;
                rotation.x = _minXAngle;
                _barrel.localEulerAngles = rotation;
            }
        }

        if (transform.eulerAngles.y > 180)
        {
            if (transform.eulerAngles.y - 360 < -_maxYAngle)
            {
                var rotation = transform.eulerAngles;
                rotation.y = -_maxYAngle;
                transform.eulerAngles = rotation;
            }
        }
        else
        {
            if (transform.eulerAngles.y > _maxYAngle)
            {
                var rotation = transform.eulerAngles;
                rotation.y = _maxYAngle;
                transform.eulerAngles = rotation;
            }
        }

        Targeting();
    }

    private void Targeting()
    {
        float angle = -_barrel.eulerAngles.x * Mathf.Deg2Rad;
        float a = 0.5f * Physics.gravity.y;
        float b = Options.ShootForce * Mathf.Sin(angle);
        float c = _shootPoint.position.y;
        float time = (-b - Mathf.Sqrt(b * b - 4 * a * c)) / (2 * a);
        float far = 15f * Mathf.Cos(angle) * time;

        var position = _shootPoint.position;
        position.y = 0.1f;
        position.z += far * Mathf.Cos(transform.eulerAngles.y * Mathf.Deg2Rad) * 1.52f;
        position.x += far * Mathf.Sin(transform.eulerAngles.y * Mathf.Deg2Rad) * 1.52f;

        _target.position = position;
    }

    private void OnLose()
    {
        enabled = false;
    }
}
