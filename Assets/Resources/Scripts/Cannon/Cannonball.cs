﻿using UnityEngine;
using DG.Tweening;
using FUGames.Pool;

[RequireComponent(typeof(Rigidbody), typeof(SphereCollider))]
public class Cannonball : Poolable
{
    [SerializeField] private LayerMask _explosionLayers;
    [SerializeField] private TrailRenderer _trail;
    [SerializeField] private ParticleSystem _smoke;
    [SerializeField] private ParticleSystem _debris;

    private Vector3 _velocity;
    private Rigidbody _rigidbody;
    private PoolManager _pool;

    private const int _bridgeLayer = 11;
    private const int _bossLayer = 14;

    enum States { InShoot, Landed, BossHit }

    private States _state;

    private void Awake()
    {
        _rigidbody = GetComponent<Rigidbody>();
        _pool = FindObjectOfType<PoolManager>();
    }

    private void FixedUpdate()
    {
        if (_state == States.Landed)
        {
            _rigidbody.velocity = _velocity;
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        switch (_state)
        {
            case States.InShoot:
                transform.localScale = Vector3.one * 0.6f;

                if (collision.gameObject.layer != _bossLayer)
                {
                    _velocity = _rigidbody.velocity;
                    _velocity.y = Physics.gravity.y;
                }

                Explode(Options.PrimaryRadius, Options.PrimaryForce);
                break;
            default:
                Explode(Options.SecondaryRadius, Options.SecondaryForce);
                break;
        }

        if (collision.gameObject.layer == _bossLayer && _state != States.BossHit)
        {
            collision.gameObject.GetComponent<Limb>().TakeDamage(1);

            _state = States.BossHit;

            CancelInvoke();
            Invoke("BackToPool", 1f);
        }

        if (collision.gameObject.layer == _bridgeLayer)
        {
            if (_state != States.Landed)
            {
                _debris.transform.parent = null;
                _debris.transform.rotation = Quaternion.Euler(Vector3.zero);
                _debris.Play();
                _smoke.Play();
                _state = States.Landed;
            }
        }
    }
   
    private void Explode(float radius, float force)
    {
        var guys = Physics.OverlapSphere(
                    transform.position,
                    radius,
                    _explosionLayers.value,
                    QueryTriggerInteraction.Collide);

        foreach (var guy in guys)
        {
            if (guy.TryGetComponent(out Enemy enemy))
            {
                enemy.HadCollision(gameObject);
            }
            else
            {
                guy.attachedRigidbody?.AddExplosionForce(
                    force,
                    transform.position,
                    radius);
            }
        }
    }

    public override void Refresh()
    {
        tag = "Cannonball";

        _debris.transform.parent = transform;
        _debris.transform.localPosition = Vector3.zero;
        _trail.enabled = true;
        _trail.endWidth = 0.25f;
        _trail.time = 0.3f;
        _state = States.InShoot;

        var sequence = DOTween.Sequence();
        sequence.Append(transform.DOScaleZ(1f, 0.1f));
        sequence.Append(transform.DOScaleZ(0.6f, 0.6f));
        sequence.Join(_trail.DOTime(0f, 0.8f));
        sequence.Join(_trail.DOResize(_trail.startWidth, 0, 0.8f));

        Invoke("BackToPool", 5f);
    }

    public override void BackToPool()
    {
        CancelInvoke();

        _trail.Clear();
        _trail.enabled = false;
        _rigidbody.velocity = Vector3.zero;
        _rigidbody.angularVelocity = Vector3.zero;
        _pool.Put(GetType(), gameObject);
    }
}
