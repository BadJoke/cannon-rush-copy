﻿using DG.Tweening;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class Settings : MonoBehaviour
{
    [SerializeField] private RectTransform _settingsPanel;
    [SerializeField] private EventSystem _eventSystem;
    [SerializeField] private Button _settingsButton;
    [SerializeField] private Slider _sensitivitySlider;
    [SerializeField] private Controller _controller;

    private void Start()
    {
        _sensitivitySlider.value = Options.SensRatio;
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            var pointerEventData = new PointerEventData(EventSystem.current);
            pointerEventData.position = Input.mousePosition;
            var raycastResults = new List<RaycastResult>();

            EventSystem.current.RaycastAll(pointerEventData, raycastResults);

            if (raycastResults.Count == 0)
            {
                CloseSettings();
            }
            else
            {
                bool needClose = true;
                foreach (var raycast in raycastResults)
                {
                    if (raycast.gameObject.CompareTag("UISettings"))
                    {
                        needClose = false;
                        break;
                    }
                }

                if (needClose)
                {
                    CloseSettings();
                }
            }
        }
    }

    private void CloseSettings()
    {
        Time.timeScale = 1f;

        transform.DOScale(Vector3.zero, 0.5f).onComplete = () =>
        {
            gameObject.SetActive(false);
            _settingsButton.enabled = true;
            _controller.enabled = true;
        };
    }

    public void OnSettingsClick()
    {
        Time.timeScale = 0f;

        _settingsPanel.gameObject.SetActive(true);
        _settingsPanel.DOScale(Vector3.one, 0.5f).SetUpdate(true);
        _settingsButton.enabled = false;
        _controller.enabled = false;
    }

    public void OnSensitivityChanged(float sensitivity)
    {
        Options.SensRatio = sensitivity;
        PlayerPrefs.SetFloat("SensRatio", Options.SensRatio);
    }
}
