﻿using DG.Tweening;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class Timer : MonoBehaviour
{
    [SerializeField] private Image _bar;
    [SerializeField] private Image _end;
    [SerializeField] private EnemyCounter _enemyCounter;
    [SerializeField] private SpellController _bonusController;
    [SerializeField] private EnemySpawner _spawner;
    [SerializeField] private BossHealthBar _healthBar;

    public event UnityAction TimeOut;
    public event UnityAction Win;

    private float _duration;
    private Color _color;
    private Sequence _sequence;

    private void OnEnable()
    {
        _enemyCounter.EnemiesDestroyed += OnEnemiesDestroyed;
        _spawner.BossSpawned += OnBossSpawned;
        _bonusController.Freeze += OnFreeze;
        _bonusController.UnFreeze += OnUnFreeze;
    }

    private void OnDisable()
    {
        _enemyCounter.EnemiesDestroyed -= OnEnemiesDestroyed; ;
        _spawner.BossSpawned -= OnBossSpawned;
        _bonusController.Freeze -= OnFreeze;
        _bonusController.UnFreeze -= OnUnFreeze;
    }

    private void Start()
    {
        _duration = UIOptions.LevelTime;
        _color = _end.color;
        _color.a = 1f;

        Tween firstDilling = _bar
            .DOFillAmount(UIOptions.SpawnDisableTime, _duration * UIOptions.SpawnDisableTime)
            .SetEase(Ease.Linear)
            .Pause();

        firstDilling.onComplete = () =>
        {
            TimeOut?.Invoke();
        };

        _sequence = DOTween.Sequence();
        _sequence.Append(firstDilling);
        _sequence.Append(_bar.DOFillAmount(1f, _duration * (1f - UIOptions.SpawnDisableTime)).SetEase(Ease.Linear));
        _sequence.SetAutoKill(false);
    }

    private void OnEnemiesDestroyed()
    {
        _bar.fillAmount = 1f;
        _end.color = _color;

        Invoke("Victory", UIOptions.VictoryDelay);
    }

    private void Victory()
    {
        Win?.Invoke();
    }

    private void OnFreeze()
    {
        if (_sequence.IsComplete() == false)
        {
            _sequence.Pause();
        }
    }

    private void OnUnFreeze()
    {
        if (_sequence.IsComplete() == false)
        {
            _sequence.Play();
        }
    }

    private void OnBossSpawned()
    {
        _healthBar.gameObject.SetActive(true);
        _healthBar.transform.DOLocalMoveY(_healthBar.transform.localPosition.y - 400, 0.5f);
        transform.DOLocalMoveY(transform.localPosition.y + 400, 0.5f);
    }
}
