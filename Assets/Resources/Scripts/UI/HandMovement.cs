﻿using DG.Tweening;
using DG.Tweening.Plugins.Core.PathCore;
using UnityEngine;

public class HandMovement : MonoBehaviour
{
    [SerializeField] private RectTransform[] _targets;

    private Vector3[] _targetPositions;
    private Tween tween;

    private void Start()
    {
        _targetPositions = new Vector3[_targets.Length];

        for (int i = 0; i < _targets.Length; i++)
        {
            _targetPositions[i] = _targets[i].position;
        }

        var path = new Path(PathType.CatmullRom, _targetPositions, _targetPositions.Length);
        tween = transform.DOPath(path, 4f);
        tween.SetLoops(-1).SetEase(Ease.Linear).SetDelay(0.5f);
    }

    private void OnDisable()
    {
        tween.Kill();
    }
}
