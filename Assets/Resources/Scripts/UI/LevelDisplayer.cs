﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using DG.Tweening;
using System.Collections;

public class LevelDisplayer : MonoBehaviour
{
    [Header("UI")]
    [SerializeField] private Text _currentLevel;
    [SerializeField] private Text _nextLevel;
    [SerializeField] private Text _textLevel;
    [SerializeField] private Text _coinsCount;
    [SerializeField] private GameObject _menu;
    [SerializeField] private EndPanel _end;
    [SerializeField] private Transform _coins;
    [SerializeField] private Transform _progressBar;
    [SerializeField] private Transform _level;
    [SerializeField] private Transform _spells;
    [SerializeField] private Transform _home;

    [Space]
    [Header("Other scripts")]
    [SerializeField] private Timer _timer;
    [SerializeField] private Fire _fire;
    [SerializeField] private Controller _controller;
    [SerializeField] private EnemySpawner _spawner;
    [SerializeField] private SpellController _spellController;
    [SerializeField] private Castle _castle;

    private bool _hasLose;

    private void OnEnable()
    {
        _timer.Win += OnWin;
        _castle.Lose += OnLose;
    }

    private void OnDisable()
    {
        _timer.Win -= OnWin;
        _castle.Lose -= OnLose;
    }

    private void Start()
    {
        if (PlayerPrefs.HasKey("Coins"))
        {
            Options.Coins = PlayerPrefs.GetInt("Coins");
        }
        else
        {
            Options.Coins = 0;
        }

        _textLevel.text = "LEVEL " + Options.CurrentLevel;
        _currentLevel.text = Options.CurrentLevel.ToString();
        _nextLevel.text = (Options.CurrentLevel + 1).ToString();
        _coinsCount.text = Options.Coins.ToString();

        _hasLose = false;

        AddCoins();
    }

    public void OnMenuClick()
    {
        _menu.SetActive(false);

        _timer.enabled = true;
        _fire.enabled = true;
        _controller.enabled = true;
        _spawner.enabled = true;
        _spellController.enabled = true;

        _coins.DOLocalMoveX(_coins.localPosition.x + 800f, 1f);
        _progressBar.DOLocalMoveY(_progressBar.localPosition.y - 400f, 1f);
        _level.DOLocalMoveY(_level.localPosition.y + 950f, 1f);
        _spells.DOLocalMoveY(_spells.localPosition.y + 700f, 1f);
        _home.DOLocalMoveX(_home.localPosition.x - 400f, 1f);
    }

    public void LoadLevel()
    {
        _end.Hide();

        Invoke("LoadScene", 0.5f);
    }

    private void LoadScene()
    {
        SceneManager.LoadScene("Gameplay");
    }

    private void AddCoins()
    {
        StartCoroutine(AddingCoins());
    }

    private IEnumerator AddingCoins()
    {
        float coins = Options.Coins;
        float maxCoins = coins + UIOptions.CoinsReward;
        float stepRatio = 0.1f;
        float step = UIOptions.CoinsReward * stepRatio;
        WaitForSeconds interval = new WaitForSeconds(stepRatio / 2);

        while (coins < maxCoins)
        {
            coins += step;

            _coinsCount.text = string.Format("{0:0}", coins);

            yield return interval;
        }

        Options.Coins = (int)maxCoins;
        PlayerPrefs.SetInt("Coins", Options.Coins);
        UIOptions.CoinsReward = 0;
    }

    private void OnWin()
    {
        if (_hasLose == false)
        {
            PlayerPrefs.SetInt("Level", ++Options.CurrentLevel);

            _end.gameObject.SetActive(true);
            _end.Show(true);
        }
    }

    private void OnLose()
    {
        _hasLose = true;
        _end.gameObject.SetActive(true);
        _end.Show(false);
    }
}
