﻿using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class BossHealthBar : MonoBehaviour
{
    [SerializeField] private Image _bar;

    private int _health;
    private int _maxHealth;
    private Boss _boss;

    private void OnEnable()
    {
        _health = Options.BossHealth;
        _maxHealth = _health;
        _bar.fillAmount = 1f;
        _boss = FindObjectOfType<Boss>();
        _boss.DamageTaken += OnDamageTaken;
    }

    private void OnDisable()
    {
        _boss.DamageTaken -= OnDamageTaken;
    }

    private void OnDamageTaken(int damage)
    {
        _health -= damage;
        _bar.DOFillAmount((float)_health / _maxHealth, 0.5f);
    }
}
