﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class SpellClickEffect : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
    [SerializeField] private Image _image;
    [SerializeField] private Image _back;
    [SerializeField] private Image _pressed;
    [SerializeField] private Image _unpressed;

    public void OnPointerDown(PointerEventData eventData)
    {
        if (_image.fillAmount == 1f)
        {
            SetIcon(_pressed.sprite, Vector3.down);
        }
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        if (gameObject.Equals(eventData.pointerCurrentRaycast.gameObject) == false)
        {
            SetAsActive();
        }
    }

    public void SetAsActive()
    {
        SetIcon(_unpressed.sprite, Vector3.up);
    }

    private void SetIcon(Sprite icon, Vector3 direction)
    {
        _image.sprite = icon;
        _back.sprite = icon;
        _back.transform.Translate(direction * 1.6f);
    }
}
