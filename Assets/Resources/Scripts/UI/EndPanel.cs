﻿using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class EndPanel : MonoBehaviour
{
    [SerializeField] private Text _message;
    [SerializeField] private Text _button;
    [SerializeField] private Text _coins;
    [SerializeField] private float _duration;

    private const string _winMessage = "LEVEL CLEARED!";
    private const string _winButtonText = "CLAIM";
    private const string _loseMessage = "YOU LOSE";
    private const string _loseButtonText = "RESTART";

    public void Show(bool win)
    {
        if (win)
        {
            _message.text = _winMessage;
            _button.text = _winButtonText;
        }
        else
        {
            _message.text = _loseMessage;
            _button.text = _loseButtonText;
        }

        _coins.text = UIOptions.CoinsReward.ToString();

        transform.DOScale(Vector3.one, _duration);
    }

    public void Hide()
    {
        transform.DOScale(Vector3.zero, _duration).onComplete = () =>
        {
            enabled = false;
        };
    }
}
