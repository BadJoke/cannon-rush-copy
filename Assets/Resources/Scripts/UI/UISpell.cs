﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using static SpellController;

public class UISpell : MonoBehaviour
{
    [SerializeField] private Image _image;
    [SerializeField] private Image _backImage;
    [SerializeField] private Image _pressedImage;
    [SerializeField] private Image _unpressedImage;

    private float _timer;
    private bool _hasCooldown;
    private SpellType _type;
    private SpellClickEffect _clickEffect;

    public event UnityAction<SpellType> UseSpell;

    private void Start()
    {
        _hasCooldown = true;
        _clickEffect = GetComponentInChildren<SpellClickEffect>();
    }

    public void SetType(SpellType type, Sprite icon, Sprite pressedIcon, float timer)
    {
        _type = type;
        _timer = timer;
        _image.sprite = pressedIcon;
        _backImage.sprite = pressedIcon;
        _pressedImage.sprite = pressedIcon;
        _unpressedImage.sprite = icon;
        _clickEffect.enabled = false;
    }

    public void UpdateTimer(float value)
    {
        if (_hasCooldown)
        {
            _image.fillAmount += value / _timer;

            if (_image.fillAmount == 1f)
            {
                _hasCooldown = false;

                _clickEffect.enabled = true;
                _clickEffect.SetAsActive();
            }
        }
    }

    public void Display(int index)
    {
        transform.parent.SetSiblingIndex(index);
        transform.parent.gameObject.SetActive(true);
    }

    public void OnClick()
    {
        if (_image.fillAmount == 1f)
        {
            UseSpell?.Invoke(_type);

            _hasCooldown = true;
            _image.fillAmount = 0f;
        }
    }
}
