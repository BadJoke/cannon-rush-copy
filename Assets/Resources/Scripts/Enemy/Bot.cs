﻿using DG.Tweening;
using UnityEngine;

public class Bot : Enemy
{
    private BoxCollider _collider;

    private new void Awake()
    {
        base.Awake();

        _cost = 5;
        _damage = 1;
        _collider = GetComponent<BoxCollider>();
    }
    
    public override void HadCollision(GameObject gameObject)
    {
        if (_isAlive)
        {
            switch (gameObject.layer)
            {
                case _cannonballLayer:
                case _trapLayer:
                case _rockLayer:
                    if (gameObject.layer == _rockLayer)
                    {
                        gameObject.GetComponent<SuperRock>().TakeDamage(_damage);
                    }

                    transform.parent = null;
                    _enemyCounter.OnEnemyDied(_cost);
                    ActivateRagdoll();
                    Invoke("DoTransparent", Options.LifeTimeAfterStrike * 0.8f);
                    break;
                case _castleLayer:
                    _isAlive = false;
                    transform.parent = null;
                    _enemyCounter.OnEnemyDied();
                    gameObject.GetComponent<Castle>().TakeDamage(_damage);
                    BackToPool();
                    break;
            }
        }
    }

    protected override void ActivateRagdoll()
    {
        _animator.enabled = false;
        _collider.enabled = false;
        _isAlive = false;

        foreach (var limb in _limbs)
        {
            limb.useGravity = true;
            limb.constraints = RigidbodyConstraints.None;
        }
    }

    public override void Refresh()
    {
        _speed = -Options.EnemySpeed;
        _animator.enabled = true;
        _collider.enabled = true;
        _isAlive = true;
        _renderer.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.On;

        for (int i = 0; i < _materials.Length; i++)
        {
            _materials[i].shader = _defaultShader[i];
            _materials[i].color = _colors[i];
        }

        for (int i = 0; i < _limbs.Length; i++)
        {
            _limbs[i].useGravity = false;
            _limbs[i].velocity = Vector3.zero;

            var transform = _limbs[i].transform;
            transform.localPosition = _limbPositions[i]; 
            transform.localRotation = _limbRotations[i]; 
        }
    }

    protected override void OnFreeze()
    {
        _speed = 0f;
        _animator.enabled = false;

        var sequence = DOTween.Sequence();

        for (int i = 0; i < _materials.Length; i++)
        {
            sequence.Join(_materials[i].DOColor(SpellOptions.FreezeColor, 0.1f));
        }

        foreach (var limb in _limbs)
        {
            limb.velocity = Vector3.zero;
            limb.angularVelocity = Vector3.zero;
            limb.constraints = RigidbodyConstraints.FreezeAll;
        }
    }

    protected override void OnUnFreeze()
    {
        if (_isAlive)
        {
            _speed = -Options.EnemySpeed;
            _animator.enabled = true;
        }

        var sequence = DOTween.Sequence();

        for (int i = 0; i < _materials.Length; i++)
        {
            sequence.Join(_materials[i].DOColor(_colors[i], 0.1f));
        }

        foreach (var limb in _limbs)
        {
            limb.constraints = RigidbodyConstraints.None;
        }
    }
}
