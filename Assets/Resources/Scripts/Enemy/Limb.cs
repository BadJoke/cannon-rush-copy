﻿using UnityEngine;

public class Limb : MonoBehaviour
{
    private Enemy parent;

    private void Start()
    {
        parent = GetComponentInParent<Enemy>();
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (parent != null)
            parent.HadCollision(collision.gameObject);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (parent != null)
            parent.HadCollision(other.gameObject);
    }

    public void TakeDamage(int damage)
    {
        if (parent is Boss boss)
        {
            boss.TakeDamage(damage);
        }
    }
}
