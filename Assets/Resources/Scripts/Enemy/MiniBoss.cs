﻿using DG.Tweening;
using UnityEngine;

public class MiniBoss : Enemy
{
    [SerializeField] protected Color _redColor;
    [SerializeField] protected float _redDuration;

    protected int _health;
    protected Color _currentColor;

    private new void Awake()
    {
        base.Awake();

        _cost = 10;
        _damage = 3;
        _health = Options.MiniBossHealth;
        _speed = -Options.MiniBossSpeed;
        _currentColor = _materials[1].color;
    }

    public override void HadCollision(GameObject gameObject)
    {
        if (_isAlive)
        {
            switch (gameObject.layer)
            {
                case _cannonballLayer:
                    if (gameObject.CompareTag("Cannonball"))
                    {
                        if (_health > 0)
                        {
                            DoRed();
                        }

                        _health--;
                        _animator.SetTrigger("Defence");

                        if (_health == 0)
                        {
                            Die();
                        }

                        gameObject.tag = "Peaceful";
                    }
                    break;
                case _trapLayer:
                case _rockLayer:
                    if (gameObject.layer == _rockLayer)
                    {
                        gameObject.GetComponent<SuperRock>().TakeDamage(_damage);
                    }

                    _health = 0;
                    Die();
                    break;
                case _castleLayer:
                    _isAlive = false;
                    transform.parent = null;
                    _enemyCounter.OnEnemyDied();
                    gameObject.GetComponent<Castle>().TakeDamage(_damage);
                    BackToPool();
                    break;
            }
        }
    }

    private void Die()
    {
        _isAlive = false;
        transform.parent = null;
        _enemyCounter.OnEnemyDied(_cost);
        ActivateRagdoll();
        Invoke("DoTransparent", Options.LifeTimeAfterStrike * 0.8f);
    }

    protected void DoRed()
    {
        var sequence = DOTween.Sequence();
        sequence.Append(_materials[1].DOColor(_redColor, 0.2f * _redDuration));
        sequence.Append(_materials[1].DOColor(_currentColor, 0.8f * _redDuration));
    }

    protected override void ActivateRagdoll()
    {
        _animator.enabled = false;

        foreach (var limb in _limbs)
        {
            limb.isKinematic = false;
            limb.useGravity = true;
        }
    }

    public override void Refresh()
    {
        _health = Options.MiniBossHealth;
        _speed = -Options.MiniBossSpeed;
        _animator.enabled = true;
        _animator.ResetTrigger("Defence");
        _isAlive = true;
        _renderer.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.On;

        for (int i = 0; i < _materials.Length; i++)
        {
            _materials[i].shader = _defaultShader[i];
            _materials[i].color = _colors[i];
        }

        for (int i = 0; i < _limbs.Length; i++)
        {
            _limbs[i].isKinematic = true;
            _limbs[i].useGravity = false;
            _limbs[i].velocity = Vector3.zero;

            var transform = _limbs[i].transform;
            transform.localPosition = _limbPositions[i];
            transform.localRotation = _limbRotations[i];
        }
    }

    protected override void OnFreeze()
    {
        if (_isAlive)
        {
            _speed = 0f;
            _animator.enabled = false;
        }

        FreezeColors();
    }

    protected void FreezeColors()
    {
        _currentColor = SpellOptions.FreezeColor;

        var sequence = DOTween.Sequence();

        for (int i = 0; i < _materials.Length; i++)
        {
            sequence.Join(_materials[i].DOColor(SpellOptions.FreezeColor, 0.1f));
        }
    }

    protected override void OnUnFreeze()
    {
        if (_isAlive)
        {
            _speed = -Options.MiniBossSpeed;
            _animator.enabled = true;
        }

        UnFreezeColors();
    }

    protected void UnFreezeColors()
    {
        _currentColor = _colors[1];

        var sequence = DOTween.Sequence();

        for (int i = 0; i < _materials.Length; i++)
        {
            sequence.Join(_materials[i].DOColor(_colors[i], 0.1f));
        }
    }
}
