﻿using DG.Tweening;
using FUGames.Pool;
using UnityEngine;

public abstract class Enemy : Poolable, IRagdoll
{
    [SerializeField] protected Shader _forDeathShader;

    protected Shader[] _defaultShader;

    protected int _cost;
    protected int _damage;
    protected float _speed;
    protected bool _isAlive;

    protected Animator _animator;
    protected Rigidbody[] _limbs;
    protected Vector3[] _limbPositions;
    protected Quaternion[] _limbRotations;
    protected PoolManager _pool;
    protected EnemyCounter _enemyCounter;
    protected SpellController _bonusController;

    protected SkinnedMeshRenderer _renderer;
    protected Material[] _materials;
    protected Color[] _colors;
    protected Color[] _transparentColors;

    protected const int _castleLayer = 12;
    protected const int _cannonballLayer = 8;
    protected const int _trapLayer = 15;
    protected const int _rockLayer = 17;

    protected void Awake()
    {
        _isAlive = true;
        _speed = -Options.EnemySpeed;
        _animator = GetComponent<Animator>();
        _limbs = GetComponentsInChildren<Rigidbody>();
        _limbPositions = new Vector3[_limbs.Length];
        _limbRotations = new Quaternion[_limbs.Length];
        _pool = FindObjectOfType<PoolManager>();
        _enemyCounter = FindObjectOfType<EnemyCounter>();
        _bonusController = FindObjectOfType<SpellController>();

        for (int i = 0; i < _limbs.Length; i++)
        {
            var transform = _limbs[i].transform;
            _limbPositions[i] = transform.localPosition;
            _limbRotations[i] = transform.localRotation;
        }

        _renderer = GetComponentInChildren<SkinnedMeshRenderer>();
        _materials = _renderer.materials;
        _colors = new Color[_materials.Length];
        _transparentColors = new Color[_materials.Length];
        _defaultShader = new Shader[_materials.Length];

        for (int i = 0; i < _materials.Length; i++)
        {
            _colors[i] = _materials[i].color;
            _transparentColors[i] = new Color(_colors[i].r, _colors[i].g, _colors[i].b, 0);
            _defaultShader[i] = _materials[i].shader;
        }

        _bonusController.Freeze += OnFreeze;
        _bonusController.UnFreeze += OnUnFreeze;
    }

    protected void FixedUpdate()
    {
        if (_isAlive)
        {
            transform.Translate(transform.forward * _speed * Time.deltaTime);
        }
    }

    protected abstract void ActivateRagdoll();

    public override void BackToPool()
    {
        Refresh();

        for (int i = 0; i < _materials.Length; i++)
        {
            _materials[i].shader = _defaultShader[i];
            _materials[i].color = _colors[i];
        }

        _pool.Put(GetType(), gameObject);
    }

    protected void DoTransparent()
    {
        _renderer.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;

        var sequence = DOTween.Sequence();

        for (int i = 0; i < _materials.Length; i++)
        {
            _materials[i].shader = _forDeathShader;
            sequence.Join(_materials[i].DOColor(_transparentColors[i], 0.2f));
        }

        sequence.onComplete = () =>
        {
            if (this is Boss)
            {
                Destroy(gameObject);
            }
            else
            {
                BackToPool();
            }
        };
    }

    public abstract void HadCollision(GameObject gameObject);

    protected abstract void OnFreeze();

    protected abstract void OnUnFreeze();

    protected void OnDestroy()
    {
        _bonusController.Freeze -= OnFreeze;
        _bonusController.UnFreeze -= OnUnFreeze;
    }
}
