﻿using UnityEngine;

public interface IRagdoll
{
    void HadCollision(GameObject gameObject);
}