﻿using DG.Tweening;
using UnityEngine;
using UnityEngine.Events;

public class Boss : MiniBoss
{
    [SerializeField] private GameObject[] _hats;
    [SerializeField] private GameObject[] _weapons;

    public event UnityAction<int> DamageTaken;

    private Sequence _sequence;

    private new void OnDestroy()
    {
        base.OnDestroy();
        _sequence.Kill();
    }

    private new void Awake()
    {
        base.Awake();

        _cost = 15;
        _damage = 20;
        _health = Options.BossHealth;
        _speed = -Options.BossSpeed;
        _currentColor = _materials[1].color;

        Random.InitState(Options.CurrentLevel);
        _hats[Random.Range(0, _hats.Length)].SetActive(true);
        _weapons[Random.Range(0, _weapons.Length)].SetActive(true);
    }

    public void TakeDamage(int damage)
    {
        if (_isAlive)
        {
            _health -= damage;

            DoRed();

            if (_health <= 0)
            {
                StartDie();
            }
        }

        DamageTaken?.Invoke(damage);
    }

    public override void HadCollision(GameObject gameObject)
    {
        if (_isAlive)
        {
            if (gameObject.CompareTag("Castle"))
            {
                gameObject.GetComponent<Castle>().TakeDamage(_damage);

                _isAlive = false;
                transform.parent = null;
                _enemyCounter.OnEnemyDied();

                Destroy(this.gameObject);
            }

            if (gameObject.layer == _rockLayer)
            {
                gameObject.GetComponent<SuperRock>().TakeDamage(_damage);
            }
        }
    }

    public override void Refresh()
    {
        _health = Options.BossHealth;
        _speed = -Options.BossSpeed;
        _animator.enabled = true;
        _isAlive = true;
        _renderer.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.On;

        for (int i = 0; i < _materials.Length; i++)
        {
            _materials[i].shader = _defaultShader[i];
            _materials[i].color = _colors[i];
        }

        for (int i = 0; i < _limbs.Length; i++)
        {
            _limbs[i].isKinematic = true;
            _limbs[i].useGravity = false;
            _limbs[i].velocity = Vector3.zero;

            var transform = _limbs[i].transform;
            transform.localPosition = _limbPositions[i];
            transform.localRotation = _limbRotations[i];
        }
    }

    protected override void OnFreeze()
    {
        if (_isAlive)
        {
            _speed = 0f;
        }

        _sequence?.Pause();
        _animator.enabled = false;

        FreezeColors();
    }

    protected override void OnUnFreeze()
    {
        if (_isAlive)
        {
            _speed = -Options.MiniBossSpeed;
        }

        _sequence?.Play();
        _animator.enabled = true;

        UnFreezeColors();
    }

    private void StartDie()
    {
        _speed = 0;
        _animator.SetTrigger("Die");
        _isAlive = false;
        transform.parent = null;
        _enemyCounter.OnEnemyDied(_cost);

        _sequence = DOTween.Sequence().PrependInterval(5.35f);
        _sequence.onComplete = () =>
        {
            DoTransparent();
        };
    }
}
