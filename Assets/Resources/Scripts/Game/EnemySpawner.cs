﻿using FUGames.Pool;
using UnityEngine;
using UnityEngine.Events;

public class EnemySpawner : MonoBehaviour
{
    [SerializeField] private Timer _timer;
    [SerializeField] private Transform _container;
    [SerializeField] private PoolManager _pool;
    [SerializeField] private SpellController _spellController;
    [SerializeField] private GameObject _bossPrefab;

    public event UnityAction BossSpawned;
    public event UnityAction SpawnStopped;

    private bool _isNotTimeOut = true;
    private int _spawnCount;
    private int _miniBossCounter;
    private Vector2Int _miniBossFrequence;
    private System.Random _random;

    private void OnEnable()
    {
        _timer.TimeOut += OnTimeOut;
        _spellController.Freeze += OnFreeze;
        _spellController.UnFreeze += OnUnFreeze;
    }

    private void OnDisable()
    {
        _timer.TimeOut -= OnTimeOut;
        _spellController.Freeze -= OnFreeze;
        _spellController.UnFreeze -= OnUnFreeze;
    }

    private void Start()
    {
        _random = new System.Random();
        _spawnCount = 1;
        _miniBossFrequence = Options.FrequenceBossSpawn;
        _miniBossCounter = _random.Next(_miniBossFrequence.x, _miniBossFrequence.y + 1);

        InvokeRepeating("Spawn", 0f, Options.EnemySpawnDelay);
    }

    private void Spawn()
    {
        if (_spawnCount % _miniBossCounter == 0)
        {
            SpawnMiniBoss();
        }
        else
        {
            SpawnBots();
        }

        _spawnCount++;
    }

    private void SpawnBots()
    {
        int width = _random.Next(2, 4);
        int length = _random.Next(2, 3);
        int offset = _random.Next(-2, 2);

        for (int i = 0; i < width; i++)
        {
            for (int j = 0; j < length; j++)
            {
                var position = new Vector3(i * 1.2f + offset, 0, j * 1.5f);
                var enemy = _pool.Take(typeof(Bot)).transform;
                enemy.position = transform.position + position;
                enemy.rotation = Quaternion.Euler(0f, 180f, 0f);
                enemy.parent = _container;
            }
        }
    }

    private void OnTimeOut()
    {
        _isNotTimeOut = false;

        CancelInvoke("Spawn");

        if (Options.CurrentLevel % 5 == 0)
        {
            Invoke("SpawnBoss", Options.EnemySpawnDelay / 2);
        }

        Stop();
    }

    private void OnFreeze()
    {
        CancelInvoke("Spawn");
    }

    private void OnUnFreeze()
    {
        if (_isNotTimeOut)
        {
            InvokeRepeating("Spawn", Options.EnemySpawnDelay / 2, Options.EnemySpawnDelay);
        }
    }

    private void SpawnMiniBoss()
    {
        int offset = _random.Next(-2, 3);

        var boss = _pool.Take(typeof(MiniBoss)).transform;
        boss.position = transform.position + Vector3.right * offset;
        boss.rotation = Quaternion.Euler(0f, 180f, 0f);
        boss.parent = _container;

        _miniBossCounter += _random.Next(_miniBossFrequence.x, _miniBossFrequence.y + 1);
    }

    private void SpawnBoss()
    {
        var boss = Instantiate(_bossPrefab, transform.position, Quaternion.Euler(0f, 180f, 0f), _container);

        BossSpawned?.Invoke();
    }

    private void Stop()
    {
        SpawnStopped?.Invoke();
    }
}
