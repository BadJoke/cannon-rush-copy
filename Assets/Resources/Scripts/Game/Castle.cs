﻿using System.Collections;
using UnityEngine;
using UnityEngine.Events;

public class Castle : MonoBehaviour
{
    [SerializeField] private int _maxHealth = 10;
    [SerializeField] private float _regenDelay;
    [SerializeField] private float _regenInterval;
    [SerializeField] private CameraShaker _shaker;
    [SerializeField] private ParticleSystem[] _rocks;

    public event UnityAction Lose;

    private int _health;
    private bool _isNotLose;

    private void Start()
    {
        _health = _maxHealth;
        _isNotLose = true;
    }

    public void TakeDamage(int damage)
    {
        if (_isNotLose)
        {
            _health -= damage;
            _shaker.StartShake();

            foreach (var rock in _rocks)
            {
                rock.Play();
            }

            if (_health <= 0)
            {
                Lose?.Invoke();
                StopAllCoroutines();
                enabled = false;
                _isNotLose = false;
                return;
            }

            StopCoroutine("Regenerate");
            StartCoroutine("Regenerate");
        }
    }

    private IEnumerator Regenerate()
    {
        yield return new WaitForSeconds(_regenDelay);

        while (_health < _maxHealth)
        {
            _health++;

            yield return new WaitForSeconds(_regenInterval);
        }
    }
}
