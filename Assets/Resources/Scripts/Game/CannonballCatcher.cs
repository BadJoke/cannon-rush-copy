﻿using UnityEngine;

public class CannonballCatcher : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Cannonball"))
        {
            other.GetComponent<Cannonball>().BackToPool();
        }
    }
}
