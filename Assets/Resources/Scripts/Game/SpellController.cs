﻿using DG.Tweening;
using FUGames.Pool;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using URandom = UnityEngine.Random;
using Random = System.Random;
using UnityEngine.Rendering.PostProcessing;

public class SpellController : MonoBehaviour
{
    [SerializeField] private Vector3 _spikeSpawnpoint;
    [SerializeField] private Vector2Int _size;
    [SerializeField] private Transform _container;
    [SerializeField] private Transform[] _asteroidPoints;
    [SerializeField] private PoolManager _pool;
    [SerializeField] private PostProcessVolume _fire;
    [SerializeField] private PostProcessVolume _freeze;

    [Space]
    [SerializeField] private UISpell[] _spells;
    [SerializeField] private Sprite[] _icons;
    [SerializeField] private Sprite[] _presssedIcons;

    public enum SpellType { SpeedUp, Spikes, Freeze, SuperRock, Fireballs }

    public event UnityAction Freeze;
    public event UnityAction UnFreeze;

    private Random _random;
    private List<Vector2> _spellPositionsList;
    private Queue<Vector2> _spellPositionsQueue;

    private Tweener _fireTweener;
    private Tweener _freezeTweener;

    private void Start()
    {
        _random = new Random();
        _spellPositionsList = new List<Vector2>();
        _spellPositionsQueue = new Queue<Vector2>();
        URandom.InitState(Options.CurrentLevel);

        StopAllCoroutines();

        int[] spells = Shuffle(new int[] { 0, 1, 2, 3, 4 }, false);

        for (int i = 0; i < _spells.Length; i++)
        {
            float timer;

            switch (spells[i])
            {
                case 0:
                    timer = SpellOptions.SpeedUpCooldown;
                    break;
                case 1:
                    timer = SpellOptions.SpikesCooldown;
                    break;
                case 2:
                    timer = SpellOptions.FreezeCooldown;
                    break;
                case 3:
                    timer = SpellOptions.SuperCannonballCooldown;
                    break;
                case 4:
                    timer = SpellOptions.FireballCooldown;
                    break;
                default:
                    throw new MissingComponentException();
            }

            _spells[i].SetType((SpellType)spells[i], _icons[spells[i]], _presssedIcons[spells[i]], timer);
            _spells[i].UseSpell += OnUseSpell;
        }

        int x = _size.x;
        int z = _size.y;

        for (float i = -x; i <= x; i += 2f)
        {
            for (float j = -z; j <= z; j += 6f)
            {
                _spellPositionsList.Add(new Vector2(i, j));
            }
        }

        _spellPositionsQueue = new Queue<Vector2>(Shuffle(_spellPositionsList.ToArray(), true));
    }

    private void Update()
    {
        float time = Time.deltaTime;

        foreach (var spell in _spells)
        {
            spell.UpdateTimer(time);
        }
    }

    private void OnUseSpell(SpellType spellType)
    {
        switch (spellType)
        {
            case SpellType.SpeedUp:
                ShootingSpeedUp();
                break;
            case SpellType.Spikes:
                SpikeTrap();
                break;
            case SpellType.Freeze:
                Freezing();
                break;
            case SpellType.SuperRock:
                SuperRock();
                break;
            case SpellType.Fireballs:
                Asteroids();
                break;
        }
    }

    private void ShootingSpeedUp()
    {
        _fireTweener?.Kill();
        StopCoroutine("ShootSpeedUping");
        StartCoroutine("ShootSpeedUping");
    }

    private void Freezing()
    {
        _freezeTweener?.Kill();
        StopCoroutine("ToFreeze");
        StartCoroutine("ToFreeze");
    }

    private void SpikeTrap()
    {
        var count = _random.Next(SpellOptions.SpikesCount.x, SpellOptions.SpikesCount.y + 1);
        SpawnSpikes(count, SpellOptions.SpikesRadius);
    }

    private void Asteroids()
    {
        int count = _random.Next(SpellOptions.FireballCount.x, SpellOptions.FireballCount.y + 1);
        StopCoroutine(SpawnAsteroids(count));
        StartCoroutine(SpawnAsteroids(count));
    }

    private void SpawnSpikes(int count, float radius)
    {
        List<Vector2> savedPoints = new List<Vector2>();
        Vector2 offset;
        float x;
        float z;

        for (int i = 0; i < count; i++)
        {
            do
            {
                offset = _spellPositionsQueue.Dequeue();
                x = offset.x;
                z = offset.y;
                _spellPositionsQueue.Enqueue(offset);
            } while (CheckDistance(offset, savedPoints, radius));

            savedPoints.Add(offset);

            var position = _spikeSpawnpoint;
            position.x += x;
            position.z += z;

            var gameObject = _pool.Take(typeof(SpikesController)).transform;
            gameObject.position = position;
            gameObject.rotation = Quaternion.identity;
            gameObject.GetComponent<Poolable>().Refresh();
        }
    }

    private IEnumerator SpawnAsteroids(int count)
    {
        List<Transform> savedPoints = new List<Transform>();

        for (int i = 0; i < count; i++)
        {
            Transform point;

            do
            {
                point = _asteroidPoints[URandom.Range(0, _asteroidPoints.Length)];
            } while (CheckPosition(point, savedPoints));

            savedPoints.Add(point);

            var gameObject = _pool.Take(typeof(Asteroid)).transform;
            gameObject.position = point.position;
            gameObject.rotation = point.rotation;
            gameObject.GetComponent<Poolable>().Refresh();

            yield return new WaitForSeconds(SpellOptions.FireballInterval);
        }
    }

    private bool CheckDistance(Vector2 point, List<Vector2> savedPoints, float radius)
    {
        for (int i = 0; i < savedPoints.Count; i++)
        {
            if (Vector2.Distance(point, savedPoints[i]) < radius)
            {
                return true;
            }
        }
        return false;
    }

    private bool CheckPosition(Transform point, List<Transform> points)
    {
        foreach (var p in points)
        {
            if (point.Equals(p))
            {
                return true;
            }
        }

        return false;
    }

    private void SuperRock()
    {
        var superRock = _pool.Take(typeof(SuperRock)).transform;
        superRock.position = new Vector3(0, 8, 5);
        superRock.parent = _container;
        superRock.GetComponent<SuperRock>().Refresh();
    }

    private IEnumerator ShootSpeedUping()
    {
        DOTween.To(w => _fire.weight = w, _fire.weight, 1f, 0.2f).Play();
        Options.CurrentShootDelay = Options.ShootDelay / SpellOptions.SpeedUpModifier;

        yield return new WaitForSeconds(SpellOptions.SpeedUpDuration);

        _fireTweener = DOTween.To(w => _fire.weight = w, _fire.weight, 0f, 0.2f).Play();
        Options.CurrentShootDelay = Options.ShootDelay;
    }

    private IEnumerator ToFreeze()
    {
        DOTween.To(w => _freeze.weight = w, _freeze.weight, 1f, 0.2f).Play().onComplete = Freeze.Invoke;

        yield return new WaitForSeconds(SpellOptions.FreezeDuration);

        _freezeTweener = DOTween.To(w => _freeze.weight = w, _freeze.weight, 0f, 0.4f).Play();
        _freezeTweener.onComplete = UnFreeze.Invoke;
    }

    private T[] Shuffle<T>(T[] ts, bool trueRand)
    {
        var count = ts.Length;
        var last = count - 1;

        for (var i = 0; i < last; ++i)
        {
            var r = trueRand ? _random.Next(i, count) : URandom.Range(i, count);
            var tmp = ts[i];
            ts[i] = ts[r];
            ts[r] = tmp;
        }

        return ts;
    }
}
