﻿using System.Collections.Generic;
using UnityEngine;

public class Water : MonoBehaviour
{
    [SerializeField] private Pool _pool;

    private Queue<GameObject> _splashes;

    private void Start()
    {
        _splashes = new Queue<GameObject>();
    }

    private void OnTriggerEnter(Collider other)
    {
        Effect(other.transform.position);
    }

    private void Effect(Vector3 position)
    {
        var splash = _pool.GetObject();

        var transform = splash.transform;
        transform.localScale = Vector3.one * 10;
        transform.position = position;
        splash.GetComponent<ParticleSystem>().Play();
        _splashes.Enqueue(splash);

        Invoke("PutSplash", 2f);
    }

    private void PutSplash()
    {
        _pool.PutObject(_splashes.Dequeue());
    }
}
