﻿using UnityEngine;
using UnityEngine.Events;

public class EnemyCounter : MonoBehaviour
{
    [SerializeField] private EnemySpawner _spawner;

    public event UnityAction EnemiesDestroyed;

    private bool _isStopped = false;

    private void OnEnable()
    {
        _spawner.SpawnStopped += OnSpawnStopped;
    }

    private void OnDisable()
    {
        _spawner.SpawnStopped -= OnSpawnStopped;
    }

    private void OnSpawnStopped()
    {
        _isStopped = true;

        if (transform.childCount == 0)
        {
            if (Options.CurrentLevel % 5 != 0)
            {
                EnemiesDestroyed?.Invoke();
            }
        }
    }

    public void OnEnemyDied(int coins = 0)
    {
        UIOptions.CoinsReward += coins;

        if (transform.childCount == 0 && _isStopped)
        {
            EnemiesDestroyed?.Invoke();
        }
    }
}
