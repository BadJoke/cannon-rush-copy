﻿using DG.Tweening;
using UnityEngine;
using UnityEngine.Events;
using static SpellController;

public class SpellBook : MonoBehaviour
{
    [SerializeField] private Animator _animator;
    [SerializeField] private Transform _book;
    [SerializeField] private SkinnedMeshRenderer _mesh;

    private float _time = 0;
    private float _timer;
    private bool _isOpen;
    private SpellType _type;

    public event UnityAction<SpellType> UseSpell;


    private void OnMouseUp()
    {
        if (_time == _timer)
        {
            _isOpen = true;
            _time = 0;
            _animator.SetTrigger("Open");
            _book.DOLocalRotate(Vector3.zero, 0.45f);
            _book.DOLocalMoveX(0f, 0.45f);
            _book.DOLocalMoveY(2f, 0.45f);

            UseSpell?.Invoke(_type);
        }
    }

    public void SetType(SpellType type, Material icon, float timer)
    {
        _isOpen = true;
        _type = type;
        _timer = timer;

        var materials = _mesh.materials;
        materials[3] = icon;
        _mesh.materials = materials;
    }

    public void UpdateTimer(float value)
    {
        if (_time < _timer)
        {
            _time += value;

            if (_time >= _timer - 0.3f && _isOpen)
            {
                _animator.SetTrigger("Close");
                _book.DOLocalRotate(Vector3.back * 90, 0.45f);
                _book.DOLocalMoveX(-0.065f, 0.45f);
                _book.DOLocalMoveY(1.85f, 0.45f);
                _isOpen = false;
            }

            if (_time >= _timer)
            {
                _time = _timer;
            }
        }
    }
}
