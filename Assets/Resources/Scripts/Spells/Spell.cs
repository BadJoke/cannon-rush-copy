﻿using FUGames.Pool;
using UnityEngine;

public class Spell : Poolable
{
    protected int _damage;

    public override void BackToPool(){}

    public override void Refresh(){}

    protected void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Boss"))
        {
            collision.gameObject.GetComponent<Limb>().TakeDamage(_damage);
        }
    }
}
