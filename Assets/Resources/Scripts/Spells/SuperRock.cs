﻿using FUGames.Pool;
using UnityEngine;

public class SuperRock : Spell
{
    private int _health;
    private Vector3 _velocity = Vector3.forward * 5;
    private Quaternion _startRotation;
    private Rigidbody _rigidbody;
    private PoolManager _pool;
    private Rock[] _rocks;
    private Collider _collider;
    private MeshRenderer _mesh;

    private const int _rockLayer = 17;
    private const int _deathLayer = 0;

    private void Awake()
    {
        _health = SpellOptions.SuperCannonballHealth;
        _damage = SpellOptions.SuperCannonballDamage;
        _startRotation = transform.rotation;
        _rigidbody = GetComponent<Rigidbody>();
        _pool = FindObjectOfType<PoolManager>();
        _rocks = GetComponentsInChildren<Rock>();
        _collider = GetComponent<SphereCollider>();
        _mesh = GetComponent<MeshRenderer>();
    }

    private void FixedUpdate()
    {
        _velocity.y = _rigidbody.velocity.y;
        _rigidbody.velocity = _velocity;

        transform.Rotate(transform.right);
    }

    private void OnTriggerExit(Collider collider)
    {
        if (collider.gameObject.CompareTag("Catcher"))
        {
            CancelInvoke();
            BackToPool();
        }
    }

    public void TakeDamage(int damage)
    {
        if (_health > 0 )
        {
            _health -= damage;

            if (_health <= 0)
            {
                Destroy();
            }
        }
    }

    public override void BackToPool()
    {
        foreach (var rock in _rocks)
        {
            rock.Change(false);
        }

        _pool.Put(GetType(), gameObject);
    }

    public override void Refresh()
    {
        transform.rotation = _startRotation;
        gameObject.layer = _rockLayer;

        _mesh.enabled = true;
        _collider.enabled = true;

        Invoke("Destroy", 7.5f);
    }

    private void Destroy()
    {
        CancelInvoke();

        _mesh.enabled = false;
        _collider.enabled = false;

        foreach (var rock in _rocks)
        {
            rock.Change(true);
        }

        gameObject.layer = _deathLayer;

        Invoke("BackToPool", 2.5f);
    }
}
