﻿using FUGames.Pool;
using UnityEngine;

public class Asteroid : Spell
{
    [SerializeField] private LayerMask _explosionLayers;
    [SerializeField] private ParticleSystem _blust;

    private CameraShaker _shaker;
    private PoolManager _pool;
    private Rock[] _rocks;
    private Collider _collider;
    private Rigidbody _rigidbody;
    private MeshRenderer _mesh;
    private Vector3 _velocity;

    private const int _trapLayer = 15;
    private const int _deathLayer = 0;

    private void Awake()
    {
        GetComponent<Rigidbody>().velocity += Vector3.down * 1.2f;

        _damage = SpellOptions.FireballDamage;
        _blust.GetComponent<ParticleSystem>();
        _shaker = FindObjectOfType<CameraShaker>();
        _pool = FindObjectOfType<PoolManager>();
        _rocks = GetComponentsInChildren<Rock>();
        _collider = GetComponent<MeshCollider>();
        _rigidbody = GetComponent<Rigidbody>();
        _mesh = GetComponent<MeshRenderer>();
    }

    private void FixedUpdate()
    {
        _rigidbody.velocity = _velocity;
    }

    private new void OnCollisionEnter(Collision collision)
    {
        Explode(SpellOptions.FireballExplosionRadius, Options.PrimaryForce);

        _blust.transform.parent = null;
        _blust.Play();
        _shaker.StartShake();

        base.OnCollisionEnter(collision);

        Destroy();
    }

    private void Explode(float radius, float force)
    {
        var guys = Physics.OverlapSphere(
                    transform.position,
                    radius,
                    _explosionLayers.value,
                    QueryTriggerInteraction.Collide);

        foreach (var guy in guys)
        {
            if (guy.TryGetComponent(out Enemy enemy))
            {
                enemy.HadCollision(gameObject);
            }
            else
            {
                guy.attachedRigidbody?.AddExplosionForce(
                    force,
                    transform.position,
                    radius);
            }
        }
    }

    public override void BackToPool()
    {
        foreach (var rock in _rocks)
        {
            rock.Change(false);
        }

        _pool.Put(GetType(), gameObject);
    }

    public override void Refresh()
    {
        gameObject.layer = _trapLayer;

        _velocity = transform.up * -SpellOptions.FireballSpeed;

        _blust.transform.parent = transform;
        _blust.transform.localPosition = Vector3.zero;

        _mesh.enabled = true;
        _collider.enabled = true;
        _rigidbody.velocity = Vector3.zero;
        _rigidbody.angularVelocity = Vector3.zero;
    }

    private void Destroy()
    {
        _mesh.enabled = false;
        _collider.enabled = false;

        foreach (var rock in _rocks)
        {
            rock.Change(true);
        }

        gameObject.layer = _deathLayer;

        Invoke("BackToPool", 2.5f);
    }
}
