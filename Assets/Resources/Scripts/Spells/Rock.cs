﻿using DG.Tweening;
using UnityEngine;

public class Rock : MonoBehaviour
{
    private Material _material;
    private Vector3 _startPosition;
    private Quaternion _startRotation;
    private Rigidbody _rigidbody;
    private Collider _collider;
    private Transform _parent;

    private void Awake()
    {
        _material = GetComponent<MeshRenderer>().material;
        _startPosition = transform.localPosition;
        _startRotation = transform.localRotation;
        _rigidbody = GetComponent<Rigidbody>();
        _collider = GetComponent<MeshCollider>();
        _parent = transform.parent;
    }

    public void Change(bool transparent)
    {
        var color = _material.color;

        if (transparent)
        {
            color.a = 0f;
            _rigidbody.constraints = RigidbodyConstraints.None;
            _rigidbody.isKinematic = false;
            transform.parent = null;
        }
        else
        {
            color.a = 1f;
            _rigidbody.constraints = RigidbodyConstraints.FreezeAll;
            _rigidbody.isKinematic = true;
            transform.parent = _parent;
            transform.localPosition = _startPosition;
            transform.localRotation = _startRotation;
        }

        _rigidbody.useGravity = transparent;
        _collider.enabled = transparent;

        Sequence sequence = DOTween.Sequence();
        sequence.PrependInterval(SpellOptions.SuperCannonballFadeInterval);
        sequence.Append(_material.DOColor(color, SpellOptions.SuperCannonballFadeDuration));
    }
}
