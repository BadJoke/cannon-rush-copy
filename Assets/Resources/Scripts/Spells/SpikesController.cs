﻿using DG.Tweening;
using FUGames.Pool;
using UnityEngine;

public class SpikesController : Spell
{
    [SerializeField] private Shader _transparent;

    private Shader _basic;
    private Material[] _materials;
    private Color[] _colors;
    private Color[] _transparentColors;
    private PoolManager _pool;

    private void Awake()
    {
        _damage = SpellOptions.SpikesDamage;
        _pool = FindObjectOfType<PoolManager>();
        _materials = GetComponent<MeshRenderer>().materials;
        _basic = _materials[0].shader;
        _colors = new Color[_materials.Length];
        _transparentColors = new Color[_materials.Length];

        for (int i = 0; i < _materials.Length; i++)
        {
            _colors[i] = _materials[i].color;
            _transparentColors[i] = _materials[i].color;
            _transparentColors[i].a = 0f;
        }
    }

    public override void BackToPool()
    {
        _pool.Put(GetType(), gameObject);
    }

    public override void Refresh()
    {
        for (int i = 0; i < _materials.Length; i++)
        {
            _materials[i].shader = _basic;
            _materials[i].color = _colors[i];
        }

        Invoke("Transparenting", SpellOptions.SpikesDuration);
    }

    private void Transparenting()
    {
        var sequence = DOTween.Sequence();

        for (int i = 0; i < _materials.Length; i++)
        {
            _materials[i].shader = _transparent;
            sequence.Join(_materials[i].DOColor(_transparentColors[i], 0.2f));
        }

        sequence.onComplete = BackToPool;
    }
}
