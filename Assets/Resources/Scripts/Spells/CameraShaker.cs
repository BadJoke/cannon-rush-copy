﻿using Cinemachine;
using System.Collections;
using UnityEngine;

public class CameraShaker : MonoBehaviour
{
    [SerializeField] private float ShakeDuration = 0.3f;          // Time the Camera Shake effect will last
    [SerializeField] private float ShakeAmplitude = 1.2f;         // Cinemachine Noise Profile Parameter
    [SerializeField] private float ShakeFrequency = 2.0f;         // Cinemachine Noise Profile Parameter

    // Cinemachine Shake
    [SerializeField] private CinemachineVirtualCamera VirtualCamera;

    private CinemachineBasicMultiChannelPerlin virtualCameraNoise;

    private void Start()
    {
        virtualCameraNoise = VirtualCamera.GetCinemachineComponent<CinemachineBasicMultiChannelPerlin>();
    }

    public void StartShake()
    {
        StopAllCoroutines();
        StartCoroutine(Shake());
    }

    private IEnumerator Shake()
    {
        virtualCameraNoise.m_AmplitudeGain = ShakeAmplitude;
        virtualCameraNoise.m_FrequencyGain = ShakeFrequency;

        yield return new WaitForSeconds(ShakeDuration);

        virtualCameraNoise.m_AmplitudeGain = 0f;
    }
}
