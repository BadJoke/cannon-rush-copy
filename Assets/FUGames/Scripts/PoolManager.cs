﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace FUGames
{
    namespace Pool
    {
        public class PoolManager : MonoBehaviour
        {
            [SerializeField] private int _startSize = 10;
            [SerializeField] private GameObject[] _prefabs;

            private Dictionary<Type, Pool> _pools;

            private void Awake()
            {
                _pools = new Dictionary<Type, Pool>();

                foreach (var prefab in _prefabs)
                {
                    var type = prefab.GetComponent<MonoBehaviour>().GetType();

                    _pools.Add(type, new Pool(prefab, _startSize, transform));
                }
            }

            public GameObject Take(Type type)
            {
                if (_pools.ContainsKey(type))
                {
                    return _pools[type].TakeObject();
                }

                return null;
            }

            public void Put(Type type, GameObject gameObject)
            {
                if (_pools.ContainsKey(type))
                {
                    _pools[type].PutObject(gameObject);
                }
            }

            private class Pool
            {
                private Queue<GameObject> _pool;
                private Transform _parent;
                private GameObject _object;

                internal Pool(GameObject gameObject, int size, Transform parent)
                {
                    _pool = new Queue<GameObject>();
                    _parent = parent;
                    _object = gameObject;

                    for (int i = 0; i < size; i++)
                    {
                        Create();
                    }
                }

                public GameObject TakeObject()
                {
                    if (_pool.Count == 0)
                    {
                        for (int i = 0; i < 1; i++)
                        {
                            Create();
                        }
                    }

                    var gameObject = _pool.Dequeue();
                    gameObject.transform.parent = null;
                    gameObject.SetActive(true);

                    return gameObject;
                }

                public void PutObject(GameObject gameObject)
                {
                    var transform = gameObject.transform;
                    transform.parent = _parent;
                    transform.localPosition = Vector3.zero;
                    transform.localRotation = Quaternion.Euler(Vector3.zero);

                    gameObject.SetActive(false);

                    _pool.Enqueue(gameObject);
                }

                private void Create()
                {
                    var obj = Instantiate(_object, _parent.position, Quaternion.identity, _parent);
                    obj.SetActive(false);

                    _pool.Enqueue(obj);
                }
            }
        }

        public abstract class Poolable : MonoBehaviour
        {
            public abstract void BackToPool();
            public abstract void Refresh();
        }
    }
}