﻿using System.Collections.Generic;
using UnityEngine;

public class Pool : MonoBehaviour
{
    [SerializeField] private GameObject _object;
    [SerializeField] private int _count = 10;

    private Queue<GameObject> _pool = new Queue<GameObject>(); 

    private void Start()
    {
        for (int i = 0; i < _count; i++)
        {
            Create();
        }
    }

    public GameObject GetObject()
    {
        if (_pool.Count == 0)
        {
            for (int i = 0; i < 5; i++)
            {
                Create();
            }
        }

        GameObject gameObject = _pool.Dequeue();
        gameObject.transform.parent = null;
        gameObject.SetActive(true);
        return gameObject;
    }

    public void PutObject(GameObject gameObject)
    {
        Transform transform = gameObject.transform;
        transform.position = this.transform.position;
        transform.parent = this.transform;
        gameObject.SetActive(false);
        _pool.Enqueue(gameObject);
    }

    private void Create()
    {
        var obj = Instantiate(
                    _object,
                    transform.position,
                    _object.transform.rotation,
                    transform);
        obj.SetActive(false);
        _pool.Enqueue(obj);
    }
}
